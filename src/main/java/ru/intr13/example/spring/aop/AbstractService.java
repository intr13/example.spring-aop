package ru.intr13.example.spring.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractService {

    protected Logger logger;

    protected AbstractService() {
        super();

        this.logger = createLogger();
    }

    private Logger createLogger() {
        return LoggerFactory.getLogger(getClass());
    }
}
