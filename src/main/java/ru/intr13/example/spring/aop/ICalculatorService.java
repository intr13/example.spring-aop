package ru.intr13.example.spring.aop;

public interface ICalculatorService extends IService {

    Integer pow(Integer data);
}
