package ru.intr13.example.spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AspectService
        extends AbstractService {

    public void before(JoinPoint joinPoint) {
        logger.info("Invoke before with args - " + Arrays.toString(joinPoint.getArgs()));
    }

    public void after(JoinPoint joinPoint) {
        logger.info("Invoke after with args - " + Arrays.toString(joinPoint.getArgs()));
    }

    public void afterReturning(JoinPoint joinPoint, Object result) {
        logger.info("Invoke after returning with args - "
                    + Arrays.toString(joinPoint.getArgs())
                    + " and result - "
                    + result);
    }

    public void afterThrowing(JoinPoint joinPoint, Throwable cause) {
        logger.info("Invoke after throwing with args - "
                    + Arrays.toString(joinPoint.getArgs())
                    + " and throwing - "
                    + cause.getMessage());
    }

    public Object around(ProceedingJoinPoint proceedingJoinPoint)
            throws Throwable {
        logger.info("Start invoke around with args - " + Arrays.toString(proceedingJoinPoint.getArgs()));
        Object result = null;
        try {
            result = proceedingJoinPoint.proceed();
            return result;
        } finally {
            logger.info("End invoke around with args - "
                        + Arrays.toString(proceedingJoinPoint.getArgs())
                        + " and result - "
                        + result);
        }
    }

    public Object aroundBase(ProceedingJoinPoint proceedingJoinPoint)
            throws Throwable {
        throw new IllegalStateException("Around base");
    }

}
