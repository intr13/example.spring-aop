package ru.intr13.example.spring.aop;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CalculatorService
        extends AbstractService
        implements ICalculatorService {

    @Override
    public Integer pow(Integer data) {
        logger.info("pow for data - " + data);
        if (data == null) {
            throw new IllegalArgumentException("Data is null!");
        }
        return (data * data);
    }

    @Override
    public String getName() {
        logger.info("Get name");
        return this.getClass().getSimpleName();
    }
}
