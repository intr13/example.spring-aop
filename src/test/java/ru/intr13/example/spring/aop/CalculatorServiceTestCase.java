package ru.intr13.example.spring.aop;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CalculatorServiceTestCase {

    @Test
    public void powTest() {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/application-configuration.xml");
        ICalculatorService service = (ICalculatorService) context.getBean("calculatorService");
        Assert.assertEquals(new Integer(16), service.pow(4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsPowTest() {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/application-configuration.xml");
        ICalculatorService service = (ICalculatorService) context.getBean("calculatorService");
        service.pow(null);
    }

    @Test(expected = IllegalStateException.class)
    public void getNameTest() {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/application-configuration.xml");
        ICalculatorService service = (ICalculatorService) context.getBean("calculatorService");
        Assert.assertEquals(CalculatorService.class.getSimpleName(), service.getName());
    }
}
